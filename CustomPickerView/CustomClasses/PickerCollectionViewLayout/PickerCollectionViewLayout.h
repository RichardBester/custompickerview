//
//  PickerCollectionViewLayout.h
//  CustomizingTabBar
//
//  Created by Sauron Black on 9/1/14.
//  Copyright (c) 2014 ThinkMobiles. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PickerCollectionViewLayout : UICollectionViewLayout

@property (assign, nonatomic) CGSize itemSize;

@end
