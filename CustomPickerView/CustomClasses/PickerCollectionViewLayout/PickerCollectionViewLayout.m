//
//  PickerCollectionViewLayout.m
//  CustomizingTabBar
//
//  Created by Sauron Black on 9/1/14.
//  Copyright (c) 2014 ThinkMobiles. All rights reserved.
//

#import "PickerCollectionViewLayout.h"
#import "LinearInterpolator.h"

static const NSInteger PickerCollectionViewLayoutVisibleItemCount = 5;

@interface PickerCollectionViewLayout ()

@property (readonly, nonatomic) CGFloat minInvisibleItemHeight;
@property (readonly, nonatomic) CGFloat extremeVisibleItemHeight;
@property (readonly, nonatomic) CGFloat minCollectionViewHeight;

@property (strong, nonatomic) NSMutableArray *attributesArray;
@property (assign, nonatomic) NSUInteger itemCount;
@property (strong, nonatomic) NSArray *linearFunctions;
@property (strong, nonatomic) NSArray *spaces;
@property (strong, nonatomic) LinearInterpolator *linearInterpolator;

/**
 used for consistent cell frames calculation
*/
@property (assign, nonatomic) CGFloat lastMaxY;

@end

@implementation PickerCollectionViewLayout

#pragma mark - Lifecycle

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setup];
    }
    return self;
}

#pragma mark - Custom Accessors

- (CGFloat)minInvisibleItemHeight
{
    return self.itemSize.height * [self.linearInterpolator valueForX:97.f];
}

- (CGFloat)extremeVisibleItemHeight
{
    return self.itemSize.height * [self.linearInterpolator valueForX:72.f];
}

- (CGFloat)minCollectionViewHeight
{
    return self.itemSize.height + self.itemSize.height * [self.linearInterpolator valueForX:42.f] * 2 + self.extremeVisibleItemHeight * 2;
}

#pragma mark - Overload

- (void)prepareLayout
{
    CGRect bounds = self.collectionView.bounds;
    NSInteger itemIndex = MAX(0, (NSInteger)(CGRectGetMinY(bounds) / self.minInvisibleItemHeight) - 1);
    self.lastMaxY = itemIndex * self.minInvisibleItemHeight;
    self.itemCount = [self.collectionView.dataSource collectionView:self.collectionView numberOfItemsInSection:0];
    
    self.attributesArray = [NSMutableArray array];
    
    for (NSInteger i = itemIndex; i < MIN(self.itemCount, itemIndex + 7); i++) {
        [self.attributesArray addObject:[self layoutAttributesForItemAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0]]];
    }
}

- (UICollectionViewLayoutAttributes *)layoutAttributesForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGRect bounds = self.collectionView.bounds;
    CGFloat space = (CGRectGetMidY(bounds) - self.itemSize.height / 2.f) - self.lastMaxY;
    CGFloat scale = [self.linearInterpolator valueForX:space];
    CGFloat height = self.itemSize.height * scale;
    
    UICollectionViewLayoutAttributes *attributes = [UICollectionViewLayoutAttributes layoutAttributesForCellWithIndexPath:indexPath];
    attributes.frame = CGRectMake(0.f, self.lastMaxY - (self.itemSize.height - height) / 2.f, self.itemSize.width, self.itemSize.height);
    attributes.transform = CGAffineTransformMakeScale(1.f, scale);
    attributes.alpha = scale;
    
    self.lastMaxY += height;
    
    return attributes;
}

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect
{
    return self.attributesArray;
}

- (CGSize)collectionViewContentSize
{
    CGFloat height = self.minCollectionViewHeight;
    self.itemCount = [self.collectionView.dataSource collectionView:self.collectionView numberOfItemsInSection:0];
    if (self.itemCount > PickerCollectionViewLayoutVisibleItemCount) {
        height += (self.itemCount - PickerCollectionViewLayoutVisibleItemCount) * self.minInvisibleItemHeight;
    }
    return CGSizeMake(self.itemSize.width, height);
}

- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
    return YES;
}

#pragma mark - Private

- (void)setup
{
    self.itemSize = CGSizeMake(66.f, 50.f);
    self.linearInterpolator = [[LinearInterpolator alloc] initWithPoints:@[[NSValue valueWithCGPoint:CGPointMake(-117.f, 0.5f)],
                                                                           [NSValue valueWithCGPoint:CGPointMake(-92.f, 0.6f)],
                                                                           [NSValue valueWithCGPoint:CGPointMake(-50.f, 0.84f)],
                                                                           [NSValue valueWithCGPoint:CGPointMake(0.f, 1.f)],
                                                                           [NSValue valueWithCGPoint:CGPointMake(42.f, 0.84f)],
                                                                           [NSValue valueWithCGPoint:CGPointMake(72.f, 0.6f)],
                                                                           [NSValue valueWithCGPoint:CGPointMake(97.f, 0.5f)]]];
}

@end
