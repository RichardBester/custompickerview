//
//  LinearFuction.h
//  CustomPickerView
//
//  Created by Sauron Black on 9/5/14.
//  Copyright (c) 2014 ThinkMobiles. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LinearFuction : NSObject

@property (assign, nonatomic) CGFloat a;
@property (assign, nonatomic) CGFloat b;

+ (LinearFuction *)linearFunctionFromStartPoint:(CGPoint)p1 endPoint:(CGPoint)p2;

- (CGFloat)valueForX:(CGFloat)x;

@end
