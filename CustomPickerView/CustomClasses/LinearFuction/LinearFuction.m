//
//  LinearFuction.m
//  CustomPickerView
//
//  Created by Sauron Black on 9/5/14.
//  Copyright (c) 2014 ThinkMobiles. All rights reserved.
//

#import "LinearFuction.h"

@implementation LinearFuction

#pragma mark - Static Public

+ (LinearFuction *)linearFunctionFromStartPoint:(CGPoint)p1 endPoint:(CGPoint)p2
{
    LinearFuction *linearFunction = [LinearFuction new];
    linearFunction.a = (p2.y - p1.y) / (p2.x - p1.x);
    linearFunction.b = (-p1.x * (p2.y - p1.y) + p1.y * (p2.x - p1.x)) / (p2.x - p1.x);
    return linearFunction;
}

#pragma mark - Public

- (CGFloat)valueForX:(CGFloat)x
{
    return self.a * x + self.b;
}

#pragma mark - Override

- (NSString *)description
{
    return [NSString stringWithFormat:@"y = %.3f * x + %.3f", self.a, self.b];
}

@end
