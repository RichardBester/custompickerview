//
//  LinearInterpolator.h
//  CustomPickerView
//
//  Created by Sauron Black on 9/10/14.
//  Copyright (c) 2014 ThinkMobiles. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LinearInterpolator : NSObject

/**
 Array of CGPoit objects. X-s sequence must be increasing.
 */
@property (strong, nonatomic) NSArray *points;

- (instancetype)initWithPoints:(NSArray *)points;

- (CGFloat)valueForX:(CGFloat)x;

@end
