//
//  LinearInterpolator.m
//  CustomPickerView
//
//  Created by Sauron Black on 9/10/14.
//  Copyright (c) 2014 ThinkMobiles. All rights reserved.
//

#import "LinearInterpolator.h"

@implementation LinearInterpolator

#pragma mark - Lifecycle

- (instancetype)initWithPoints:(NSArray *)points
{
    self = [super init];
    if (self) {
        self.points = points;
    }
    return self;
}

#pragma mark - Custom Accessors

- (void)setPoints:(NSArray *)points
{
    NSAssert([points count] > 1, @"Number of points must be greater than 1.");
    
    BOOL increasing = YES;
    for (NSInteger i = 0; (i < [points count] - 1) && increasing; i++) {
        CGPoint p0 = [(NSValue *)points[i] CGPointValue];
        CGPoint p1 = [(NSValue *)points[i + 1] CGPointValue];
        increasing = p0.x < p1.x;
    }
    
    NSAssert(increasing, @"X-s sequence must be increasing.");
    
    _points = points;
}

#pragma mark - Public

- (CGFloat)valueForX:(CGFloat)x
{
    CGPoint p0;
    CGPoint p1;
    CGFloat y;
    
    if ([self getStartPoint:&p0 endPoint:&p1 forX:x]) {
        CGFloat a = (p1.y - p0.y) / (p1.x - p0.x);
        CGFloat b = -a * p0.x + p0.y;
        y = a * x + b;
    } else {
        y = p1.y;
    };
    
    return y;
}

#pragma mark - Private;

- (BOOL)getStartPoint:(CGPoint *)startPoint endPoint:(CGPoint *)endPoint forX:(CGFloat)x
{
    CGPoint p0;
    CGPoint p1;
    BOOL found = NO;
    
    for (NSInteger i = 0; (i < [self.points count] - 1) && !found; i++) {
        p0 = [(NSValue *)self.points[i] CGPointValue];
        p1 = [(NSValue *)self.points[i + 1] CGPointValue];
        found = (x >= p0.x) && (x <= p1.x);
    }
    
    *startPoint = p0;
    *endPoint = p1;
    
    return found;
}

@end
