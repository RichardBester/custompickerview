//
//  PickerCollectionViewCell.m
//  CustomizingTabBar
//
//  Created by Sauron Black on 9/1/14.
//  Copyright (c) 2014 ThinkMobiles. All rights reserved.
//

#import "PickerCollectionViewCell.h"

@implementation PickerCollectionViewCell

+ (NSString *)cellReuseIdentifier {
    static NSString *PickerCollectionViewCellReuseIdentifier = @"PickerCollectionViewCellReuseIdentifier";
    return PickerCollectionViewCellReuseIdentifier;
}

@end
