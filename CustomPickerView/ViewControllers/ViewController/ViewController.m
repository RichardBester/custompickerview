//
//  ViewController.m
//  CustomPickerView
//
//  Created by Sauron Black on 9/5/14.
//  Copyright (c) 2014 ThinkMobiles. All rights reserved.
//

#import "ViewController.h"
#import "LinearFuction.h"

@interface ViewController ()

@property (weak, nonatomic) IBOutlet UITextField *x1TextField;
@property (weak, nonatomic) IBOutlet UITextField *y1TextField;
@property (weak, nonatomic) IBOutlet UITextField *x2TextField;
@property (weak, nonatomic) IBOutlet UITextField *y2TextField;
@property (weak, nonatomic) IBOutlet UITextField *xTextField;
@property (weak, nonatomic) IBOutlet UILabel *resultFuctionLable;
@property (weak, nonatomic) IBOutlet UILabel *resultValueLabel;

@property (strong, nonatomic) LinearFuction *linearFunction;

@end

@implementation ViewController

#pragma mark - Private

- (void)createLinearFunctionForX1:(CGFloat)x1 y1:(CGFloat)y1 x2:(CGFloat)x2 y2:(CGFloat)y2
{
    self.linearFunction = [LinearFuction linearFunctionFromStartPoint:CGPointMake(x1, y1) endPoint:CGPointMake(x2, y2)];
    self.resultFuctionLable.text = [self.linearFunction description];
}

#pragma mark - Touches

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

#pragma mark - IBActions;

- (IBAction)calculateAction:(UIButton *)sender
{
    [self createLinearFunctionForX1:[self.x1TextField.text floatValue]  y1:[self.y1TextField.text floatValue] x2:[self.x2TextField.text floatValue] y2:[self.y2TextField.text floatValue]];
}

- (IBAction)xTextFieldDidEndEditing:(UITextField *)sender
{
    self.resultValueLabel.text = [NSString stringWithFormat:@"y = %.3f", [self.linearFunction valueForX:[sender.text floatValue]]];
}

@end
