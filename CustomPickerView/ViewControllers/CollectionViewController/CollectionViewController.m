//
//  CollectionViewController.m
//  CustomPickerView
//
//  Created by Sauron Black on 9/5/14.
//  Copyright (c) 2014 ThinkMobiles. All rights reserved.
//

#import "CollectionViewController.h"
#import "PickerCollectionViewCell.h"

@interface CollectionViewController () <UICollectionViewDataSource, UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation CollectionViewController

#pragma mark - Lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 60;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PickerCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:[PickerCollectionViewCell cellReuseIdentifier] forIndexPath:indexPath];
    
    cell.textLabel.text = [@(indexPath.item % 30) stringValue];
    
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat y = scrollView.contentOffset.y;
    CGFloat height = scrollView.contentSize.height;
    
    if (y < (height / 60.f)) {
        scrollView.contentOffset = CGPointMake(0.f, y + height / 2.f);
    } else if (y > ((height * 50) / 60.f)) {
        scrollView.contentOffset = CGPointMake(0.f, y - height / 2.f);
    }
//    if (scrollView.contentOffset.y > scrollView.contentSize.height / 3.f * 2.f + 10.f) {
//        scrollView.contentOffset = CGPointMake(0.f, scrollView.contentSize.height / 3.f + 25.f);
//    } else  if (scrollView.contentOffset.y < scrollView.contentSize.height / 3.f - 10.f) {
//        scrollView.contentOffset = CGPointMake(0.f, scrollView.contentSize.height / 3.f * 2.f - 25.f);
//    }
}

@end
